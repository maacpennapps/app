
if (Meteor.isClient) {
  Users = new Mongo.Collection('users');
  // counter starts at 0
  Session.setDefault('counter', 0);
  Template.hello.helpers({
    counter: function () {
      return Session.get('counter');
  }});

  Template.hello.events({
    'click button': function () {
      // increment the counter when button is clicked
      Session.set('counter', Session.get('counter') + 1);
    }
  });

  Template.SignUp.events({
    'submit #signup-form' : function(e, t){
       e.preventDefault();
      var email = t.find('#account-email').value
        , password = t.find('#account-password').value;

        // Trim and validate the input

      Accounts.createUser({email: email, password : password}, function(err){
          if (err) {
            // Inform the user that account creation failed
          } else {
            // Success. Account has been created and the user
            // has logged in successfully. 
          }

        });

      return false;
    }
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
     var result = HTTP.call("POST", "https://tartan.plaid.com/connect",
                           {data: {client_id:"test_id", secret:"test_secret",
                            username:"plaid_test", password:"plaid_good", type:"wells"}});
    // code to run on server at startup

})}
Transactions = new Mongo.Collection("Transactions"); 
Budgets = new Mongo.Collection("Budgets");
Users = new Mongo.Collection("Users");
var ctx = document.getElementById("myChart").getContext("2d");
var myNewChart = new Chart(ctx).PolarArea(data);
if (Meteor.isClient) {
  // counter starts at 0
  Session.setDefault('counter', 0);
  Template.hello.helpers({
    counter: function () {
      return Session.get('counter');
    }
  });

  Template.hello.events({
    'click button': function () {
      // increment the counter when button is clicked
      Session.set('counter', Session.get('counter') + 1);
    }
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}

